import 'dart:convert';

import 'package:coronavirus_app/app/services/api.dart';
import 'package:coronavirus_app/app/services/endpoint_data.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

class APIService{
  APIService(this.api);
  final API api;

  Future<String> getAccessToken() async{
    final response = await http.post(
      api.tokenUri(),
      headers: {'Authorization': 'Basic ${api.apiKey}', 'Access-Control-Allow-Origin': '*'},
    );
    if (response.statusCode==200){
      final data = json.decode(response.body);
      final accessToken = data['access_token'];
      if(accessToken!=null){
        return accessToken;
      }
    }
    throw response;
  }

  Future<EndpointData> getEndpointData({
    @required String accessToken,
    @required Endpoint endpoint,
  }) async {
    final uri = api.endpointUri(endpoint);
    final response = await http.get(
      uri,
      headers: {'Authorization': 'Bearer $accessToken', 'Access-Control-Allow-Origin': '*'},
    );
    if(response.statusCode == 200){
      final List<dynamic> data = json.decode(response.body);
      if(data.isNotEmpty){
        final Map<String, dynamic> endpointData = data[0];
        final String responseJsonKey = _responseJsonKeys[endpoint];
        final int value = endpointData[responseJsonKey];
        final String dateString = endpointData['date'];
        final date = DateTime.tryParse(dateString);
        if(value != null){
          return EndpointData(value: value, date: date);
        }
      }
    }
    throw response;
  }

  static Map<Endpoint, String> _responseJsonKeys = {
    Endpoint.cases: 'cases',
    Endpoint.casesSuspected: 'data',
    Endpoint.casesConfirmed: 'data',
    Endpoint.deaths: 'data',
    Endpoint.recovered: 'data'
  };
}